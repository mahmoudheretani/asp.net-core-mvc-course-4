﻿namespace PracticalProject.Models
{
    public class BookSet
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
    }
}
