﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PracticalProject.Models
{
    public class TransactionSet
    {
        public int Id { get; set; }
        public double Amount { get; set; }
        public bool IsWidthdraw { get; set; }
        public DateTimeOffset DateTime { get; set; }
        public string AccountId { get; set; }
    }
}
