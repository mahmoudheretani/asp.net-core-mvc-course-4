﻿using Microsoft.AspNetCore.Mvc;
using PracticalProject.Models;
using PracticalProject.Util;
using System.Collections.Generic;

namespace PracticalProject.Controllers
{
    public class HomeController : Controller
    {

        #region Properties

        #endregion

        #region Constructors

        public HomeController()
        {
        }

        #endregion

        #region Methods

        public string HelloWorld()
        {
            return "Hello world from asp.net core mvc";
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Books()
        {
            ViewData["Books"] = GetBooks();
            return View();
        }

        private List<BookSet> GetBooks()
        {
            return new List<BookSet>()
            {
                new BookSet()
                {
                    Id=1,
                    Title="C++ for begineers",
                    Price=500
                },
                new BookSet()
                {
                    Id=2,
                    Title="Java Pro",
                    Price=200
                }
            };
        }

        public IActionResult Login()
        {
            return View(ViewsPaths.Login);
        }

        public IActionResult Report()
        {
            return View();
        }

        public IActionResult RedirectToStudents()
        {
            return RedirectToRoute("idWithTitle", new
            {
                controller="Book",
                action="Students",
                id=7,
                title="A"
            });
        }

        #endregion

    }
}
