﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PracticalProject.Models;
using PracticalProject.ViewModels.Transaction;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PracticalProject.Controllers
{
    public class TransactionController : Controller
    {
        public IActionResult Index()
        {
            var vm = new IndexViewModel()
            {
                Transactions=GetTransactionsFromDatabase()
            };
            return View(vm);
        }

        public IActionResult GetTransactionsData()
        {
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Ok(GetTransactionsFromDatabase());
        }

        public IActionResult SetStatusCode()
        {
            return StatusCode((int)HttpStatusCode.NotImplemented, "Error");
        }

        public IActionResult ViewTransaction()
        {
            return Content("<h2>test</h2>", "text/html");
        }

        public IActionResult GetTransactionById(int id)
        {
            TransactionSet transactionEntity = null;
            foreach (var transaction in GetTransactionsFromDatabase())
            {
                if(transaction.Id== id)
                {
                    transactionEntity = transaction;
                    break;
                }
            }

            if (transactionEntity == null)
            {
                return NotFound($"Transaction with Id {id} doesn't exist");
            }
            return Ok(transactionEntity);
        }

        private List<TransactionSet> GetTransactionsFromDatabase()
        {
            return new List<TransactionSet>()
            {
                new TransactionSet()
                {
                    Id=1,
                    DateTime=DateTimeOffset.Now,
                    AccountId="AAAA",
                    Amount=1000,
                    IsWidthdraw=true
                },
                 new TransactionSet()
                {
                    Id=2,
                    DateTime=DateTimeOffset.Now,
                    AccountId="BBBB",
                    Amount=-666,
                    IsWidthdraw=false
                }
            };
        }
    }
}
