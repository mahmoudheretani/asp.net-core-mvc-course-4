﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using PracticalProject.Util;

namespace PracticalProject.Controllers
{
    public class FileController : Controller
    {
        private IHostingEnvironment HostingEnvironment { get;  }
        public FileController(IHostingEnvironment hostingEnvironment)
        {
            HostingEnvironment = hostingEnvironment;
        }
        public IActionResult Download()
        {
          return  this.DownloadFile
                (HostingEnvironment.WebRootPath
                , @"data\book.pdf"
                , "book file.pdf"
                , "application/pdf");
        }
    }
}
