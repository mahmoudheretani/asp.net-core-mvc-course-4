﻿namespace PracticalProject.Controllers
{
    /// <summary>
    /// POCO controller
    /// </summary>
    public class StudentController
    {

        #region Properties

        #endregion

        #region Constructors

        #endregion

        #region Methods

        public string GetName()
        {
            return "Student name";
        }

        #endregion

    }
}
