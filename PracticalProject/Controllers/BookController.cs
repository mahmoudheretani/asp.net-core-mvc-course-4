﻿using Microsoft.AspNetCore.Mvc;
using PracticalProject.Results;
using System.Net;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PracticalProject.Controllers
{
    public class BookController : Controller
    {

        #region Properties
        public int MyProperty { get; set; }

        #endregion

        #region Constructors

        #endregion

        #region Methods

        public IActionResult TotalCounts => Content("1000");


        public IActionResult BadRequestAction()
        {
            Response.StatusCode = (int)HttpStatusCode.BadRequest;
            return Content("Bad request code :(");
        }

        public IActionResult HtmlResult()
        {
            var htmlResult = new HtmlResult()
            {
                Content = "<h2>This is html</h2>"
            };
            return htmlResult;
        }

        public IActionResult Students(int id, string title)
        {
            return Content($"Id :{id}, Query: {title}");
        }

        #endregion

    }
}
