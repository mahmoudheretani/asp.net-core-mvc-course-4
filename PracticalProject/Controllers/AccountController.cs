﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PracticalProject.Util;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace PracticalProject.Controllers
{
    public class AccountController : Controller
    {
        public IActionResult Login(int type)
        {
            if (type == 0)
            {
                return RedirectToAction(nameof(ProfileController.Index)
                    , nameof(ProfileController).ToActionName());
                //ExtensionMethods.ToActionName(nameof(ProfileController)
            }
            return View();
        }
    }
}
