﻿using PracticalProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PracticalProject.ViewModels.Transaction
{
    public class IndexViewModel
    {
        public List<TransactionSet> Transactions { get; set; }
        public List<BookSet> Books { get; set; }
    }
}
