﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PracticalProject.Util
{
    public static class ExtensionMethods
    {
        public static string ToActionName(this string controllerName)
        {
            return controllerName.Replace("Controller", string.Empty);
        }

        public static FileResult DownloadFile(this Controller controller, string
                wwwroot, string filePath, string fileName, string contentType)
        {
            IFileProvider provider = new PhysicalFileProvider(wwwroot);
            IFileInfo fileInfo = provider.GetFileInfo(filePath);
            var readStream = fileInfo.CreateReadStream();
            return controller.File(readStream, contentType, fileName);
        }
    }
}
